package com.company;

import com.company.figures.figures;
import com.company.figures.rectangle;
import com.company.figures.square;
import com.company.figures.triangle;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите сторону (высоту) : ");
        int side1 = in.nextInt();
        System.out.print("Введите сторону (основание) : ");
        int side2 = in.nextInt();

        rectangle rectangle = new rectangle(side1, side2);
        System.out.print("Площадь прямоугольника = ");
        rectangle.getarea();

        square square = new square(side1, side2);
        System.out.print("Площадь квадрата = ");
        square.getarea();

        triangle triangle = new triangle(side1, side2);
        System.out.print("Площадь прямоугольного треугольника = ");
        triangle.getarea();

    }
}
